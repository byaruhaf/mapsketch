//
//  NotificationsService.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 01/11/2023.
//

import Foundation
import UserNotifications
import UIKit

@MainActor
class NotificationsService: ObservableObject {
    @Published private(set) var hasPermission = false
    
    init() {
        Task {
            await getAuthStatus()
        }
    }
    
    func request() async {
        do {
            hasPermission = try await UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound])
            if hasPermission {
                UIApplication.shared.registerForRemoteNotifications()
            }
        } catch {
            print(error)
        }
    }
    
    func getAuthStatus() async {
        let status = await UNUserNotificationCenter.current().notificationSettings()
        switch status.authorizationStatus {
            
        case .authorized,
                .provisional,
                .ephemeral:
            hasPermission = true
        default:
            hasPermission = false
        }
    }
}
