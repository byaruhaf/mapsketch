//
//  DataStorageManager.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import FirebaseFirestore

class DataStorageManager: ObservableObject {

    static var shared = DataStorageManager()

    @Published var users: [User] = [] // not including current user
    @Published var allUsers: [User] = []

    func getUsers() async {
        let snapshot = try? await Firestore.firestore()
            .collection(Collection.users)
            .getDocuments()
        storeUsers(snapshot)
    }

    func subscribeToUpdates() {
        Firestore.firestore()
            .collection(Collection.users)
            .addSnapshotListener { [weak self] (snapshot, _) in
                guard let self else { return }
                self.storeUsers(snapshot)
            }
    }

    private func storeUsers(_ snapshot: QuerySnapshot?) {
        guard let currentUser = SessionManager.currentUser else { return }
        DispatchQueue.main.async { [weak self] in
            let users: [User] = snapshot?.documents
                .compactMap { document in
                    let dict = document.data()
                    if document.documentID == currentUser.id {
                        return nil // skip current user
                    }
                    if let name = dict["nickname"] as? String {
                        let avatarURL = dict["avatarURL"] as? String
                        return User(id: document.documentID, name: name, avatarURL: URL(string: avatarURL ?? ""), isCurrentUser: false)
                    }
                    return nil
                } ?? []

            self?.users = users
            self?.allUsers = users + [currentUser]
        }
    }

}

