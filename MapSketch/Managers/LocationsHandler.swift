//
//  LocationsHandler.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import MapKit

@MainActor class LocationsHandler: ObservableObject {
    
    static let shared = LocationsHandler()
    public let manager: CLLocationManager

    private init() {
        self.manager = CLLocationManager()
        if self.manager.authorizationStatus == .notDetermined {
            self.manager.requestWhenInUseAuthorization()
        }
    }
}
