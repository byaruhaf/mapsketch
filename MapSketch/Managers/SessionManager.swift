//
//  SessionManager.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import ExyteChat
import UIKit

let hasCurrentUserSessionKey = "hasCurrentUserSessionKey"
let currentUserKey = "currentUser"

let currentJourneySyncSessionKey = "currentJourneySyncSessionKey"
let hasCurrentJourneySyncSessionKey = "hasCurrentJourneySyncSessionKey"


class SessionManager {

    static let shared = SessionManager()

    static var currentUserId: String {
        shared.currentUser?.id ?? ""
    }

    static var currentUser: User? {
        shared.currentUser
    }

    var deviceId: String {
        UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    
    static var currentJourneySyncSessionId: String? {
        shared.currentJourneySyncSessionId
    }


    @Published private var currentUser: User?
    @Published private var currentJourneySyncSessionId: String?


    func storeUser(_ user: User) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(user) {
            UserDefaults.standard.set(encoded, forKey: currentUserKey)
        }
        UserDefaults.standard.set(true, forKey: hasCurrentUserSessionKey)
        currentUser = user
    }
    
    func storeJourneySyncSession(_ id: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(id) {
            UserDefaults.standard.set(encoded, forKey: currentJourneySyncSessionKey)
        }
        UserDefaults.standard.set(true, forKey: hasCurrentJourneySyncSessionKey)
        currentJourneySyncSessionId = id
    }

    func loadUser() {
        if let data = UserDefaults.standard.data(forKey: "currentUser") {
            currentUser = try? JSONDecoder().decode(User.self, from: data)
        }
    }
    
    func loadJourneySyncSession() {
        if let data = UserDefaults.standard.data(forKey: currentJourneySyncSessionKey) {
            currentJourneySyncSessionId = try? JSONDecoder().decode(String.self, from: data)
        }
    }
    
    func logout() {
        currentUser = nil
        UserDefaults.standard.set(false, forKey: hasCurrentUserSessionKey)
        UserDefaults.standard.removeObject(forKey: currentUserKey)
    }

    func leaveJourneySyncSession() {
        currentJourneySyncSessionId = nil
        UserDefaults.standard.set(false, forKey: hasCurrentJourneySyncSessionKey)
        UserDefaults.standard.removeObject(forKey: currentJourneySyncSessionKey)
    }
}

