//
//  GoogleGenerativeAIViewModel.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 01/11/2023.
//

import Foundation
import GoogleGenerativeAI

@Observable
class GoogleGenerativeAIServiceManager {
    let palmClient = GenerativeLanguage(apiKey: "APIKey")
    
    let intentStartPrompt = """
You are the world's foremost voice assistant. Upon receiving a sentence from the user, your task is to determine a user command from the command options below.
You can only return the commands listed below, if you can't determine the user's exact command then return the command that the user most likely intended to say use pronunciation to determine the intent. all text returned must be plain text and return just the command nothing else.

The allowed commands are
    red,
    blue,
    green,
    black,
    balance
The user command is:  Make it read
"""
    
    let conversationStartPrompt = """
       You are Sofia, the user's personal assistant that has access to only the transactions knowledge in this prompt. use the transactions below, to answer the question using only that information, with the short answer outputted in a conversational format as though you are responding to a friend. If the answer is not explicitly written in the transactions, say "I am sorry, but I don't have access to this information.”  give a concise answer. Do NOT waffle around.
    """
 

    @MainActor
    func generateAnswer(userPrompt: String, userTransactions: String) async -> String {
        print("Genrating Answer for \(userPrompt)")
        do {
            print("waiting for Answer from Google AI")
            let wholePromt = "\(conversationStartPrompt) \n \(userPrompt) \n \(userTransactions)"
            
            // let response = try await palmClient.chat(message: wholePromt)
            // if let candidate = response.candidates?.first, let text = candidate.content {
            //   print(text)
            //     return text
            // }
            
            let response = try await palmClient.generateText(with: "\(wholePromt)")
            dump(response)
            if let candidate = response.candidates?.first, let text = candidate.output {
                print(text)
                return text
            }
            
            print("Genrating Answer completed")
            return ""
        } catch {
            print("Genrating Answer Failed")
            print(error.localizedDescription)
            return ""
        }
    }
    
    @MainActor
    func generateIntent(userPrompt: String) async -> String{
        print("Genrating Intent for \(userPrompt)")
        do {
            print("waiting for Respose from Google AI")
            // let response = try await palmClient.generateText(with: "\(intentStartPrompt)")
            // dump(response)
            // if let candidate = response.candidates?.first, let text = candidate.output {
            //   print(text)
            //     return text
            // }
            
            let response = try await palmClient.chat(message: intentStartPrompt)
            if let candidate = response.candidates?.first, let text = candidate.content {
                print(text)
                return text
            }
            
            
            print("Genrating Document completed")
            return ""
        } catch {
            print("Genrating Document Failed")
            print(error.localizedDescription)
            return ""
        }
    }
    
}



