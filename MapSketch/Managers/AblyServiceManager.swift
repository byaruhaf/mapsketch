//
//  AblyServiceManager.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import MapKit
import Ably
import Combine
import ExyteChat
import SwiftUI

@Observable
class AblyServiceManager  {
    var allUsers: Set<User> = []
    var messages: [Message] = []
    // basic auth with an API key
    let client = ARTRealtime(key: "API-Key")
    var mapChannel: ARTRealtimeChannel?
    var chatChannel: ARTRealtimeChannel?
    var pushChannel: ARTRealtimeChannel?

    var lock = NSRecursiveLock()
    
    var clientStatus: String = ""
    private var authStateHandler: ARTEventListener?
    
    var pins = [Pin]()
    var visibleRegion : MKCoordinateRegion? = nil
    
    var cameraPosition: MapCameraPosition {
        if pins.isEmpty {
            return MapCameraPosition.automatic
        } else {
            if let last = pins.last {
                let region =  MKCoordinateRegion(center: CLLocationCoordinate2DMake(last.lat, last.lon), latitudinalMeters: 50, longitudinalMeters: 50)
                return MapCameraPosition.region(region)
                
            } else {
                return MapCameraPosition.automatic
            }
        }
    }
    
    
    
    var subscriptions = Set<AnyCancellable>()
    var tasks = Set<Task<Void, Never>>()
    
    func reset() {
        pins = []
    }
    
    func addPin(pin: Pin) {
        pins.append(pin)
    }
    
    func registerConnecitonStateHandler() {
        if authStateHandler == nil {
            authStateHandler = client.connection.on { [weak self] stateChange in
                guard let self = self else { return }
                
                let stateChange = stateChange
                
                switch stateChange.current {
                case .connected:
                    clientStatus = "connected"
                case .failed:
                    if let reason = stateChange.reason {
                        clientStatus = "failed! \(reason)"
                    } else {
                        clientStatus = "failed"
                    }
                case .initialized:
                    clientStatus = "initialized"
                case .connecting:
                    clientStatus = "connecting"
                case .disconnected:
                    clientStatus = "disconnected"
                case .suspended:
                    clientStatus = "suspended"
                case .closing:
                    clientStatus = "closing"
                case .closed:
                    clientStatus = "closed"
                @unknown default:
                    clientStatus = "unknown"
                }
            }
        }
    }
    
    init() {
        registerConnecitonStateHandler()
    }
    
    func setUpChatSubscription(clientId:String, channelName: String, user: User) {
        print("xxxxxxxxxxxxxxxxxxxxxxxx")
        print("setUpChatSubscription")
        print("xxxxxxxxxxxxxxxxxxxxxxxx")
        chatChannel = client.channels.get("chat:\(channelName)")
        
        chatChannel?.subscribe { [self] message in
//            guard let self = self else { return }

            Task {
            print(message.name ?? "")
            print(message.data ?? "")
            //            let str = message.data as! String
            if let str = message.data as? String {
                // Use the `str` variable safely here
                let decoder = JSONDecoder()
                if let decoded = try? decoder.decode(MapSketchMessage.self, from: str.data(using: .utf8)!) {
                    let newMSG = decoded.toChatMessage()
                    if newMSG.user.id != user.id {
                        self.messages.append(newMSG)
                    }
                }
            } else {
                // Handle the case where `message.data` cannot be converted to a String
                print("message.data is not a String")
            }
        }
        }
    }
    
    func setUpPushSubscription(clientId:String, channelName: String, user: User) {
        print("xxxxxxxxxxxxxxxxxxxxxxxx")
        print("setUpPushSubscription")
        print("xxxxxxxxxxxxxxxxxxxxxxxx")
        pushChannel = client.channels.get("push:\(channelName)")
        print(clientId)
        pushChannel?.subscribe { [weak self] message in
            guard let self = self else { return }
            print(message.name ?? "No Name")
            print(message.data ?? "No Data")
            //            let str = message.data as! String
            if let str = message.data as? String {
                // Use the `str` variable safely here
                dump(str)
//                let decoder = JSONDecoder()
//                if let decoded = try? decoder.decode(MapSketchMessage.self, from: str.data(using: .utf8)!) {
//                    let newMSG = decoded.toChatMessage()
//                    if newMSG.user.id != user.id {
//                        messages.append(newMSG)
//                    }
//                }
            } else {
                // Handle the case where `message.data` cannot be converted to a String
                print("message.data is not a String")
            }
        }
    }
    
    func setUpMapSubscription(clientId:String, channelName: String, user: User) {
        print("xxxxxxxxxxxxxxxxxxxxxxxx")
        print("setUpMapSubscription")
        print("xxxxxxxxxxxxxxxxxxxxxxxx")
        mapChannel = client.channels.get(channelName)
        setUpPresenceSubscription()
        mapChannel?.subscribe { [weak self] message in
            guard let self = self else { return }
            print(message.name ?? "")
            print(message.data ?? "")
//            let str = message.data as! String
            guard let str = message.data as? String else {
                // Handle the case where `message.data` cannot be converted to a String
                print("message.data is not a String")
                return
            }
            let decoder = JSONDecoder()
            if let decoded = try? decoder.decode([Pin].self, from: str.data(using: .utf8)!) {
                print(decoded)
                pins = decoded
            }
        }
        
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(user) {
            // save `encoded` somewhere
            if let json = String(data: encoded, encoding: .utf8) {
                mapChannel?.presence.enterClient(clientId, data: json) { error in
                    if let error = error {
                        print(error.statusCode)
                        print(error.message)
                        print(error.localizedDescription)
                    }
                }
            }
        }
        
        mapChannel?.presence.get { members, error in
                let allUsersCount = self.allUsers.count
                if allUsersCount == 0 {
                    var newAllUsers: [User] = []
                    members?.forEach({ member in
                        let str = member.data as! String
                        let decoder = JSONDecoder()
                        if let decoded = try? decoder.decode(User.self, from: str.data(using: .utf8)!) {
                            newAllUsers.append(decoded)
                        }
                    })
                    self.allUsers = self.allUsers.union(Set(newAllUsers))
                }
        }
        getMapMessagesHistory()
    }
    
    func publishMapData(title: String, message: String)  {
        print("xxxxxxxxxxxxxxxxxxxxxxxx")
        print("publishMapData")
        print("xxxxxxxxxxxxxxxxxxxxxxxx")
        mapChannel?.publish(title, data: message)
    }
    
    func publishMapPresenceData(message: String)  {
        // Enter this client with data and update once entered
        mapChannel?.presence.enter(message) { error in
            if let error = error {
                print(error.statusCode)
                print(error.message)
                print(error.localizedDescription)
            }
        }
    }
    
    func getMapMessagesHistory() {
        print("Running getMapMessagesHistory")
        mapChannel?.history { [weak self] (result, error) in
            guard let self = self else { return }
            guard error == nil else {
                if let error = error {
                    print(error.statusCode)
                    print(error.message)
                    print(error.localizedDescription)
                }
                return
            }
            guard let items = result?.items else { return }
            if let firstItem = items.first {
//                let str = firstItem.data as! String
                guard let str = firstItem.data as? String else {
                    // Handle the case where `message.data` cannot be converted to a String
                    print("message.data is not a String")
                    return
                }
                let decoder = JSONDecoder()
                if let decoded = try? decoder.decode([Pin].self, from: str.data(using: .utf8)!) {
                    pins = decoded
                }
            }
        }
    }
    
    func setUpPresenceSubscription() {
        // Subscribe to presence enter events
        mapChannel?.presence.subscribe(.enter) { member in
//            print(member.data ?? "")  prints "not moving"
//            let str = member.data as! String
            guard let str = member.data as? String else {
                // Handle the case where `message.data` cannot be converted to a String
                print("message.data is not a String")
                return
            }
            let decoder = JSONDecoder()
            if let decoded = try? decoder.decode(User.self, from: str.data(using: .utf8)!) {
                self.allUsers.insert(decoded)
            }
        }
        
        mapChannel?.presence.subscribe(.leave) { member in
            print(member.data ?? "") // prints "not moving"
        }
    }
    
    func sendChatMessage(_ draft: DraftMessage ) {
        Task {
            /// precreate message with fixed id and .sending status
            guard let user = SessionManager.currentUser else { return }
            let id = UUID().uuidString
            let message = await Message.makeMessage(id: id, user: user, status: .sent, draft: draft)
            messages.append(message)
            let mapSketchUser = MapSketchUser(uid: user.id, name: user.name, avatar: user.avatarURL)
            /// convert to MapSketchMessage: replace users with userIds, upload medias and get urls, replace urls with strings
            let mapSketchMessage = await draft.toMockMessage(user: mapSketchUser)
            dump(mapSketchMessage)
            
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(mapSketchMessage) {
                // save `encoded` somewhere
                if let json = String(data: encoded, encoding: .utf8) {
                    chatChannel?.publish("chat data", data: json)
                }
            }
        }
    }
}


extension DraftMessage {
    func makeMockImages() async -> [MapSketchImage] {
        await medias
            .filter { $0.type == .image }
            .asyncMap { (media : Media) -> (Media, URL?, URL?) in
                (media, await media.getThumbnailURL(), await media.getURL())
            }
            .filter { (media: Media, thumb: URL?, full: URL?) -> Bool in
                thumb != nil && full != nil
            }
            .map { media, thumb, full in
                MapSketchImage(id: media.id.uuidString, thumbnail: thumb!, full: full!)
            }
    }

    func makeMockVideos() async -> [MapSketchVideo] {
        await medias
            .filter { $0.type == .video }
            .asyncMap { (media : Media) -> (Media, URL?, URL?) in
                (media, await media.getThumbnailURL(), await media.getURL())
            }
            .filter { (media: Media, thumb: URL?, full: URL?) -> Bool in
                thumb != nil && full != nil
            }
            .map { media, thumb, full in
                MapSketchVideo(id: media.id.uuidString, thumbnail: thumb!, full: full!)
            }
    }

    func toMockMessage(user: MapSketchUser, status: Message.Status = .read) async -> MapSketchMessage {
        MapSketchMessage(
            uid: id ?? UUID().uuidString,
            sender: user,
            createdAt: createdAt,
            text: text,
            images: await makeMockImages(),
            videos: await makeMockVideos(),
            recording: recording,
            replyMessage: replyMessage
        )
    }
}
