//
//  UsersViewModel.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import FirebaseFirestore

class UsersViewModel: ObservableObject {

    @Published var searchableText: String = ""

    // group creation
    @Published var selectedUsers: [User] = []
    @Published var picture: Media?
    @Published var title: String = ""

    var filteredUsers: [User] {
        if searchableText.isEmpty {
            return dataStorage.users
        }
        return dataStorage.users.filter {
            $0.name.lowercased().contains(searchableText.lowercased())
        }
    }
}

