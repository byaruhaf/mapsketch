//
//  Constants.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import SwiftUI
import ExyteChat
import ExyteMediaPicker

struct Collection {
    static let users = "users"
    static let conversations = "conversations"
    static let messages = "messages"
}

extension String {
    static var avatarPlaceholder = "avatarPlaceholder"
    static var placeholderAvatar = "placeholderAvatar"
    static var checkSelected = "checkSelected"
    static var checkUnselected = "checkUnselected"
    static var groupChat = "groupChat"
    static var imagePlaceholder = "imagePlaceholder"
    static var navigateBack = "navigateBack"
    static var newChat = "newChat"
    static var photoIcon = "photoIcon"
    static var searchCancel = "searchCancel"
    static var searchIcon = "searchIcon"
    static var steve = "steve"
}

var dataStorage = DataStorageManager.shared

public typealias User = ExyteChat.User
public typealias Message = ExyteChat.Message
public typealias Recording = ExyteChat.Recording
public typealias Media = ExyteMediaPicker.Media
