//
//  CLLocationCoordinate2D+Extension.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import MapKit

extension CLLocationCoordinate2D {
    static let apple = CLLocationCoordinate2D(latitude: 37.334886, longitude: -122.008988)
}
