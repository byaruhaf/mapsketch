//
//  MapSketchApp.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 16/10/2023.
//

import SwiftUI
import FirebaseCore
import SwiftData

@main
struct MapSketchApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    var body: some Scene {
        WindowGroup {
            RootView()
        }.modelContainer(for: Destination.self)
    }
}


extension MapSketchApp {
    func handleDeeplinking(channelId: String) async {
        print(channelId)
        // Will use the ChatChannelView below after fixing navigation
        // ChatChannelView(viewFactory: CustomViewFactory.shared, channelController: chatClient.channelController(for: try! ChannelId(cid: channelId)))
    }
}
