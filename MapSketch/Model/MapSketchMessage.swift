//
//  MapSketchMessage.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 30/10/2023.
//

import Foundation
import ExyteChat

struct MapSketchMessage: Codable {
    let uid: String
    let sender: MapSketchUser
    let createdAt: Date

    let text: String
    let images: [MapSketchImage]
    let videos: [MapSketchVideo]
    let recording: Recording?
    let replyMessage: ReplyMessage?
}

extension MapSketchMessage {
    func toChatMessage() -> ExyteChat.Message {
        ExyteChat.Message(
            id: uid,
            user: sender.toChatUser(),
            status: .sent,
            createdAt: createdAt,
            text: text,
            attachments: images.map { $0.toChatAttachment() } + videos.map { $0.toChatAttachment() },
            recording: recording,
            replyMessage: replyMessage
        )
    }
}

struct MapSketchImage: Codable {
    let id: String
    let thumbnail: URL
    let full: URL

    func toChatAttachment() -> Attachment {
        Attachment(
            id: id,
            thumbnail: thumbnail,
            full: full,
            type: .image
        )
    }
}

struct MapSketchVideo: Codable {
    let id: String
    let thumbnail: URL
    let full: URL

    func toChatAttachment() -> Attachment {
        Attachment(
            id: id,
            thumbnail: thumbnail,
            full: full,
            type: .video
        )
    }
}


struct MapSketchUser: Equatable, Codable {
    let uid: String
    let name: String
    let avatar: URL?

    init(uid: String, name: String, avatar: URL? = nil) {
        self.uid = uid
        self.name = name
        self.avatar = avatar
    }
}

extension MapSketchUser {
    var isCurrentUser: Bool {
        uid == "1"
    }
}

extension MapSketchUser {
    func toChatUser() -> ExyteChat.User {
        ExyteChat.User(id: uid, name: name, avatarURL: avatar, isCurrentUser: isCurrentUser)
    }
}


struct MockMSG {
    
    static let dateFormatter = ISO8601DateFormatter()
    
    static let md1  = "2023-10-02T10:44:00+0000"
    static let md2  = "2023-10-03T10:44:00+0000"
    static let md3  = "2023-10-04T10:44:00+0000"
    static let md4  = "2023-10-04T10:44:00+0000"
    static let md5  = "2023-10-05T10:44:00+0000"
    static let md6  = "2023-10-06T10:44:00+0000"
    static let md7  = "2023-10-07T10:44:00+0000"
    static let md8  = "2023-10-09T10:44:00+0000"
    static let md9  = "2023-10-08T10:44:00+0000"
    static let md10 = "2023-10-11T10:44:00+0000"
    static let md11 = "2023-10-12T10:44:00+0000"
    static let md12 = "2023-10-14T10:44:00+0000"
    static let md13 = "2023-10-15T10:44:00+0000"
    static let md14 = "2023-10-16T10:44:00+0000"
    static let md15 = "2023-10-17T10:44:00+0000"
    static let md16 = "2023-10-19T10:44:00+0000"
    static let md17 = "2023-10-20T10:44:00+0000"
    static let md18 = "2023-10-21T10:44:00+0000"
    static let md19 = "2023-10-22T10:44:00+0000"
    static let md20 = "2023-10-24T10:44:00+0000"
    static let md21 = "2023-10-25T10:44:00+0000"
    static let md22 = "2023-10-26T10:44:00+0000"
    static let md23 = "2023-10-27T10:44:00+0000"
    static let md24 = "2023-10-28T10:44:00+0000"
    static let md25 = "2023-09-28T10:44:00+0000"

    
    static let d1 = dateFormatter.date(from:md1)!
    static let d2 = dateFormatter.date(from:md2)!
    static let d3 = dateFormatter.date(from:md3)!
    static let d4 = dateFormatter.date(from:md4)!
    static let d5 = dateFormatter.date(from:md5)!
    static let d6 = dateFormatter.date(from:md6)!
    static let d7 = dateFormatter.date(from:md7)!
    static let d8 = dateFormatter.date(from:md8)!
    static let d9 = dateFormatter.date(from:md9)!
    static let d10 = dateFormatter.date(from:md10)!
    static let d11 = dateFormatter.date(from:md11)!
    static let d12 = dateFormatter.date(from:md12)!
    static let d13 = dateFormatter.date(from:md13)!
    static let d14 = dateFormatter.date(from:md14)!
    static let d15 = dateFormatter.date(from:md15)!
    static let d16 = dateFormatter.date(from:md16)!
    static let d17 = dateFormatter.date(from:md17)!
    static let d18 = dateFormatter.date(from:md18)!
    static let d19 = dateFormatter.date(from:md19)!
    static let d20 = dateFormatter.date(from:md20)!
    static let d21 = dateFormatter.date(from:md21)!
    static let d22 = dateFormatter.date(from:md22)!
    static let d23 = dateFormatter.date(from:md23)!
    static let d24 = dateFormatter.date(from:md24)!
    static let d25 = dateFormatter.date(from:md25)!
    
    static let MU1 = MapSketchUser(uid: UUID().uuidString, name: "Franklin", avatar: URL(string: "https://firebasestorage.googleapis.com:443/v0/b/mapsketch-f7e5b.appspot.com/o/66132D61-26F2-44C1-BD33-1CE03D63CDB0.jpg?alt=media&token=02167f6a-e03c-4f50-9ec8-d7730aeb8624"))

    static let MU2 = MapSketchUser(uid: UUID().uuidString, name: "JoJo", avatar: URL(string: "https://firebasestorage.googleapis.com:443/v0/b/mapsketch-f7e5b.appspot.com/o/C702EC00-260C-450F-9F34-F4AA429892AC.jpg?alt=media&token=63963530-88ce-4321-92fc-0cca81948c72"))
    
    static let MU3 = MapSketchUser(uid: UUID().uuidString, name: "CoCO", avatar: URL(string: "https://firebasestorage.googleapis.com:443/v0/b/mapsketch-f7e5b.appspot.com/o/2A658468-1693-47E5-9CD0-09EE6D81D29F.jpg?alt=media&token=14327e47-9327-4f60-8626-51b19ed34716"))
    
    static var allmsg: [MapSketchMessage] = [
        MapSketchMessage(uid: UUID().uuidString, sender: MU1, createdAt: d1, text: "hi", images: [], videos: [], recording: nil, replyMessage: nil),
        MapSketchMessage(uid: UUID().uuidString, sender: MU2, createdAt: d2, text: "hi", images: [], videos: [], recording: nil, replyMessage: nil),
        MapSketchMessage(uid: UUID().uuidString, sender: MU3, createdAt: d3, text: "hi", images: [], videos: [], recording: nil, replyMessage: nil),
    ]
}
