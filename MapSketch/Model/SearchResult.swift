//
//  SearchResult.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 01/11/2023.
//

import Foundation
import MapKit

struct SearchResult: Identifiable, Hashable {
    let id = UUID()
    let location: CLLocationCoordinate2D

    static func == (lhs: SearchResult, rhs: SearchResult) -> Bool {
        lhs.id == rhs.id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
