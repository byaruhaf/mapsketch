//
//  TransportOptions.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 01/11/2023.
//

import Foundation
import SwiftUI
import MapKit

enum TransportOptions: String, Identifiable, CaseIterable {
    case automobile
    case walking
    
    var id: String {
        self.rawValue
    }
    
    var type: MKDirectionsTransportType {
        switch self {
        case .automobile:
            MKDirectionsTransportType.automobile
        case .walking:
            MKDirectionsTransportType.walking
        }
    }
}
