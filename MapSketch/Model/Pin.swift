//
//  Pin.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import MapKit

struct Pin : Codable, Identifiable, Equatable {
    private(set) var id = UUID()
    let title : String
    let lat : Double
    let lon : Double
    var loc : CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
    let red : Double
    let green : Double
    let blue : Double
}
