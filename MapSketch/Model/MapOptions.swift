//
//  MapOptions.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import SwiftUI
import MapKit

enum MapOptions: String, Identifiable, CaseIterable {
    case standard
    case hybrid
    case imagery
    
    var id: String {
        self.rawValue
    }
    
    var mapStyle: MapStyle {
        switch self {
        case .standard:
            MapStyle.standard(elevation: .automatic, pointsOfInterest: PointOfInterestCategories.all, showsTraffic: true)
        case .hybrid:
            MapStyle.hybrid(elevation: .automatic, pointsOfInterest: PointOfInterestCategories.all, showsTraffic: true)
        case .imagery:
            MapStyle.imagery(elevation: .automatic)
        }
    }
}
