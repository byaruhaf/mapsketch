//
//  SearchCompletions.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 31/10/2023.
//

import Foundation
import MapKit

struct SearchCompletions: Identifiable {
    let id = UUID()
    let title: String
    let subTitle: String
    // New property to hold the URL if it exists
    var url: URL?
}
