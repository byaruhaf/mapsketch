//
//  Destination.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 01/11/2023.
//

import Foundation
import SwiftData
import MapKit

@Model
class Destination {
    init(name: String, location: String, orderNumber: Int, lat: Double, lon: Double) {
        self.name = name
        self.location = location
        self.orderNumber = orderNumber
        self.lat = lat
        self.lon = lon
    }
    
    var name: String
    var location: String
    var orderNumber: Int
    let lat : Double
    let lon : Double
    var loc : CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
}
