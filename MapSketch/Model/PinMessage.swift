//
//  PinMessage.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation

struct PinMessage: Codable {
    let pins: [Pin]
    let count: Int
}
