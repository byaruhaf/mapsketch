//
//  CenterMessage.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 30/10/2023.
//

import Foundation

struct CenterMessage: Codable {
    let lat : Double
    let lon : Double
}
