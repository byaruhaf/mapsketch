//
//  RootView.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import SwiftUI

struct RootView: View {

    @AppStorage(hasCurrentUserSessionKey) var hasCurrentSession = false
    @AppStorage(hasCurrentJourneySyncSessionKey) var hasCurrentJourneySyncSession = false
    var ablyService = AblyServiceManager()

    var body: some View {
        Group {
            if hasCurrentSession {
                if hasCurrentJourneySyncSession {
                    HomeView(ablyService: ablyService)
                } else {
                    JourneySyncSessionView()
                }
            } else {
                AuthView()
            }
        }
        .onAppear {
            if hasCurrentSession {
                SessionManager.shared.loadUser()
                if hasCurrentJourneySyncSession {
                    SessionManager.shared.loadJourneySyncSession()
                }
            }
        }
    }
    
}
