//
//  JourneySyncSessionView.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import SwiftUI

class JourneySyncSessionViewModel: ObservableObject {
}

struct JourneySyncSessionView: View {
    @State private var showingNewSessionAlert = false
    @State private var showingJoinSessionAlert = false
    @State private var sessionID = ""
    
    var body: some View {
        VStack {
            ContentUnavailableView("No Session", systemImage: "map", description: Text("You are curently Not in a JourneySync Session"))
            VStack {
                Button {
                    showingJoinSessionAlert.toggle()
                } label: {
                    Text("Join Session")
                }
                .buttonStyle(.borderedProminent)
                Button {
                    newSessionStart()
                } label: {
                    Text("New Session")
                }
                .buttonStyle(.borderedProminent)
            }
            .padding(.bottom, 40)
        }
        .alert("Join Session", isPresented: $showingJoinSessionAlert) {
            TextField("Enter session ID", text: $sessionID).autocorrectionDisabled().textInputAutocapitalization(.never)
            Button("Join", action: joinSession)
        } message: {
            Text("Enter your \n Session ID")
        }
    }
    
    func newSessionStart() {
        sessionID = UUID().uuidString
        print("xxxxxxxxxxxxxxxxxxxxx")
        print(sessionID)
        print("xxxxxxxxxxxxxxxxxxxxx")
        SessionManager.shared.storeJourneySyncSession(sessionID)
    }
    
    func joinSession() {
        SessionManager.shared.storeJourneySyncSession(sessionID)
    }
}
