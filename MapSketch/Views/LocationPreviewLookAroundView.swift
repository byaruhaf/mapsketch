//
//  LocationPreviewLookAroundView.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 29/10/2023.
//

import Foundation
import SwiftUI
import MapKit

struct LocationPreviewLookAroundView: View {
    @State private var lookAroundScene: MKLookAroundScene?
    var pin : Pin
    
    func getLookAroundScene() {
        lookAroundScene = nil
        Task {
            let request = MKLookAroundSceneRequest(coordinate: pin.loc)
            lookAroundScene = try? await request.scene
        }
    }
    
    var body: some View {
        LookAroundPreview(initialScene: lookAroundScene)
            .overlay(alignment: .bottomTrailing) {
                Text("Look Around")
                    .font(.caption)
                    .foregroundStyle(.white)
                    .padding(18)
            }
            .onAppear {
                getLookAroundScene()
            }
            .onChange(of: pin) {
                getLookAroundScene()
            }
    }
}


struct LocationPreviewLookAroundView2: View {
    @State private var lookAroundScene: MKLookAroundScene?
    var selectedLocation : SearchResult
    
    func getLookAroundScene() {
        lookAroundScene = nil
        Task {
            let request = MKLookAroundSceneRequest(coordinate: selectedLocation.location)

            lookAroundScene = try? await request.scene
        }
    }
    
    var body: some View {
        LookAroundPreview(initialScene: lookAroundScene)
            .overlay(alignment: .bottomTrailing) {
                Text("Look Around")
                    .font(.caption)
                    .foregroundStyle(.white)
                    .padding(18)
            }
            .onAppear {
                getLookAroundScene()
            }
            .onChange(of: selectedLocation) {
                getLookAroundScene()
            }
    }
}
