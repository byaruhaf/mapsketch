//
//  MapView.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 31/10/2023.
//

import SwiftUI
import ExyteChat
import MapKit
import SwiftData

struct MapView: View {
    // @State private var position: MapCameraPosition = .userLocation(followsHeading: true, fallback: .automatic)
    @State private var position: MapCameraPosition = .automatic
    @State private var selectedPin: Pin? = nil
    @State private var longPressLocation = CGPoint.zero
    @State private var customLocation = MapLocation(id: UUID(), latitude: 0, longitude: 0)
    
    @State private var sessionID = ""
    
    @State var locationsHandler = LocationsHandler.shared
    
    @State private var randomRed = Double.random(in: 0...1)
    @State private var randomGreen = Double.random(in: 0...1)
    @State private var randomBlue = Double.random(in: 0...1)
    
    @State var messages: [Message] = []
    
    @State private var selectedMapOptions: MapOptions = .hybrid
    
    var ablyService: AblyServiceManager
    
    @State private var isSheetPresented: Bool = false
    @State private var searchResults = [SearchResult]()
    @State private var selectedLocation: SearchResult?
    
    @State private var isShowingPresence: Bool = false
    
    @Environment(\.modelContext) var modelContext
    
    @Namespace var mapScope
    
    var body: some View {
        NavigationStack {
            ZStack {
                MapReader { proxy in
                    Map(position: $position, selection: $selectedLocation, scope: mapScope) {
                        
                        ForEach(ablyService.pins) { pin in
                            Annotation(pin.title, coordinate: pin.loc) {
                                ZStack {
                                    Circle()
                                        .fill( Color(red: pin.red, green: pin.green, blue: pin.blue))
                                    Image(systemName: "mappin")
                                        .font(.largeTitle)
                                        .foregroundColor(.white)
                                        .blendMode(.difference)
                                        .padding(8)
                                }
                                .onTapGesture {
                                    print("Tap \(pin)")
                                    if   selectedPin == nil {
                                        selectedLocation = nil
                                        selectedPin = pin
                                    } else {
                                        selectedPin = nil
                                    }
                                }
                            }
                        }
                        
                        ForEach(searchResults) { result in
                            Marker(coordinate: result.location) {
                                Image(systemName: "mappin")
                            }
                            .tag(result)
                        }
                        
                    }
                    .mapStyle(selectedMapOptions.mapStyle)
                    .onAppear {
                        let currentUserId = SessionManager.currentUserId
                        guard let currentUser = SessionManager.currentUser else { return }
                        guard let currentJourneySyncSessionId = SessionManager.currentJourneySyncSessionId else { return }
                        ablyService.setUpMapSubscription(clientId: currentUserId, channelName: currentJourneySyncSessionId, user: currentUser)
                        ablyService.setUpChatSubscription(clientId: currentUserId, channelName: currentJourneySyncSessionId, user: currentUser)
                        ablyService.setUpPushSubscription(clientId: currentUserId, channelName: currentJourneySyncSessionId, user: currentUser)
                        sessionID = currentJourneySyncSessionId
                    }
                    .safeAreaInset(edge: .bottom) {
                        HStack {
                            Spacer()
                            VStack(spacing: 0) {
                                if let selectedPin {
                                    LocationPreviewLookAroundView(pin: selectedPin)
                                        .frame(height: 128)
                                        .clipShape(RoundedRectangle(cornerRadius: 10))
                                        .padding()
                                }
                                //                                if let selectedLocation {
                                //                                    LocationPreviewLookAroundView2(selectedLocation: selectedLocation)
                                //                                        .frame(height: 128)
                                //                                        .clipShape(RoundedRectangle(cornerRadius: 10))
                                //                                        .padding()
                                //                                }
                                
                            }
                        }
                        .background(.thinMaterial)
                    }
                    .mapControls {
                        MapScaleView()
                    }
                    .onMapCameraChange { context in
                        ablyService.visibleRegion = context.region
                        // print("context.region.center \(context.region.center)")
                    }
                    .gesture(LongPressGesture(minimumDuration: 0.2).sequenced(before: DragGesture(minimumDistance: 0, coordinateSpace: .local))
                        .onEnded { val in
                            switch val {
                            case .second(true, let drag):
                                let loc = drag?.location ?? .zero
                                longPressLocation = loc
                                guard let customLocation = proxy.convert(longPressLocation, from: .local) else { return}
                                guard let name = SessionManager.currentUser?.name else { return }
                                ablyService.addPin(pin: Pin(title: name, lat: customLocation.latitude, lon: customLocation.longitude, red: randomRed, green: randomGreen, blue: randomBlue))
                                let encoder = JSONEncoder()
                                if let encoded = try? encoder.encode(ablyService.pins) {
                                    // save `encoded` somewhere
                                    if let json = String(data: encoded, encoding: .utf8) {
                                        ablyService.publishMapData(title: "map data", message: json)
                                    }
                                }
                            default:
                                break
                            }
                        }
                    )
                    .highPriorityGesture(DragGesture(minimumDistance: 10))
                }
            }
            .toolbar {
                Button {
                    print("Save Destinations")
                    savePins()
                } label: {
                    Image(systemName: "square.and.arrow.down")
                }
                
                Menu {
                    Picker("Map Styles", selection: $selectedMapOptions) {
                        ForEach(MapOptions.allCases) { mapOption in
                            Text(mapOption.rawValue.capitalized).tag(mapOption)
                        }
                    }
                    .pickerStyle(.inline)
                    Divider()
                    
                    Button {
                        isShowingPresence.toggle()
                    } label: {
                        Label(isShowingPresence ? "Hide Presence" : "Show Presence", systemImage: isSheetPresented ? "eye.slash" : "eye.fill")
                    }
                    
                    Divider()
                    
                    if !sessionID.isEmpty {
                        ShareLink(item: sessionID,
                                  subject: Text("Here is your JourneySync Session invite ID"),
                                  message: Text("")) {
                            Label("Share JourneySync ID", systemImage:  "square.and.arrow.up")
                        }
                    }
                    
                    Divider()
                    
                    Button {
                        isSheetPresented.toggle()
                    } label: {
                        Label(isSheetPresented ? "Hide Search" : "Show Search", systemImage: isSheetPresented ? "eye.slash" : "magnifyingglass")
                    }
                    
                    Divider()

                    Button(role: .destructive) {
                        SessionManager.shared.leaveJourneySyncSession()
                    } label: {
                        Label("Leave Session", systemImage: "person.crop.circle.badge.xmark.fill")
                    }

                    Divider()
                    
                    Button(role: .destructive) {
                        SessionManager.shared.logout()
                    } label: {
                        Label("Leave logout", systemImage: "person.crop.circle.badge.xmark.fill")
                    }

                    Divider()
                    
                } label: {
                    Label("Menu", systemImage: "ellipsis.circle")
                }
            }
            .overlay(alignment: .topLeading, content: {
                if isShowingPresence {
                    HStack {
                        ForEach(Array(ablyService.allUsers), id: \.self) { user in
                            VStack {
                                AvatarView(url: user.avatarURL, size: 64)
                                Text(user.name)
                                    .foregroundStyle(.secondary)
                            }
                            .padding(8)
                        }
                        Spacer()
                    }
                    .background(.ultraThinMaterial)
                    .frame(height: 100)
                }
            })
            .overlay(alignment: .topTrailing) {
                if !isShowingPresence {
                    VStack {
                        MapUserLocationButton(scope: mapScope)
                        MapCompass(scope: mapScope).mapControlVisibility(.visible)
                        MapPitchToggle(scope: mapScope)
                    }
                    .padding(.trailing, 4)
                    .padding(.top, 4)
                    .buttonBorderShape(.circle)
                }
            }
            .mapScope(mapScope)
            .sheet(isPresented: $isSheetPresented) {
                SearchSheetView(searchResults: $searchResults)
                    .presentationDetents([.height(200)])
                    .presentationBackground(.regularMaterial)
                    .presentationBackgroundInteraction(.enabled(upThrough: .large))
            }
            .onChange(of: ablyService.pins, { _, _ in
                position = ablyService.cameraPosition
            })
            .onChange(of: selectedLocation) {
                selectedPin = nil
                isSheetPresented = selectedLocation == nil
            }
            .onChange(of: searchResults) {
                if let firstResult = searchResults.first, searchResults.count == 1 {
                    selectedLocation = firstResult
                }
                position = .automatic
            }
            .onAppear {
                guard let latitude = locationsHandler.manager.location?.coordinate.latitude else { return }
                guard let longitude = locationsHandler.manager.location?.coordinate.longitude else { return }
                if ablyService.pins.isEmpty {
                    ablyService.pins = [
                        //                            Pin(title: "Apple", lat: CLLocationCoordinate2D.apple.latitude, lon: CLLocationCoordinate2D.apple.longitude, red: randomRed, green: randomGreen, blue: randomBlue)
                        Pin(title: "Current Location", lat: latitude, lon: longitude, red: randomRed, green: randomGreen, blue: randomBlue)
                    ]
                }
                
            }
        }
    }
    
    func savePins() {
        do {
            try modelContext.delete(model: Destination.self)
            for (index, pin) in ablyService.pins.enumerated() {
                let destinaiton = Destination(name: pin.title, location: "Some Location", orderNumber: index, lat: pin.lat, lon: pin.lon)
                modelContext.insert(destinaiton)
            }
        } catch {
            print("Failed to delete students.")
        }
    }
}

#Preview {
    MapView(ablyService: AblyServiceManager())
}
