//
//  DestinationDetailsView.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 01/11/2023.
//

import SwiftUI
import MapKit

struct DestinationDetailsView: View {
    var destination: Destination
    
    @State private var selectedResult: MKMapItem?
    @State private var route: MKRoute?
    @State private var travelTime: String?

    @State var locationsHandler = LocationsHandler.shared
    @State private var selectedMapOptions: MapOptions = .standard
    @State private var selectedTransportType: TransportOptions = .automobile

    private let gradient = LinearGradient(colors: [.red, .orange], startPoint: .leading, endPoint: .trailing)
    private let stroke = StrokeStyle(lineWidth: 5, lineCap: .round, lineJoin: .round, dash: [8, 8])

    var startingPoint: CLLocationCoordinate2D {
        if let latitude = locationsHandler.manager.location?.coordinate.latitude, let longitude = locationsHandler.manager.location?.coordinate.longitude {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            return CLLocationCoordinate2D( latitude: destination.lat, longitude: destination.lon - 0.5)
        }
    }
    
    var destinationCoordinates: CLLocationCoordinate2D {
        CLLocationCoordinate2D(
        latitude: destination.lat,
        longitude: destination.lon
    )
    }
   
    init(destination: Destination) {
        self.destination = destination
    }

    
    var body: some View {
        Map(selection: $selectedResult) {
            // Adding the marker for the starting point
            Marker("Start", coordinate: self.startingPoint)
            Marker("End", coordinate: self.destinationCoordinates)
            // Show the route if it is available
            if let route {
                MapPolyline(route)
                    .stroke(.blue, lineWidth: 8)
            }
        }
        .mapControls {
            MapUserLocationButton()
            MapCompass()
            MapScaleView()
            MapPitchToggle()
        }
        .mapStyle(selectedMapOptions.mapStyle)
        .overlay(alignment: .bottom, content: {
            HStack {
                if let travelTime {
                    Text("Travel time: \(travelTime)")
                        .padding()
                        .font(.headline)
                        .foregroundStyle(.black)
                        .background(.ultraThinMaterial)
                        .cornerRadius(15)
                }
            }
        })
        .onChange(of: selectedResult){
            getDirections()
        }
        .onChange(of: selectedTransportType){
            getDirections()
        }
        .onAppear {
            self.selectedResult = MKMapItem(placemark: MKPlacemark(coordinate: self.destinationCoordinates))
        }
        .toolbar {
            Menu {
                    Picker("Map Styles", selection: $selectedMapOptions) {
                        ForEach(MapOptions.allCases) { mapOption in
                            Text(mapOption.rawValue.capitalized).tag(mapOption)
                        }
                    }
                    .pickerStyle(.inline)
                
                Divider()
                
                Picker("Transport Type", selection: $selectedTransportType) {
                    ForEach(TransportOptions.allCases) { typeOption in
                        Text(typeOption.rawValue.capitalized).tag(typeOption)
                    }
                }
                .pickerStyle(.inline)


            } label: {
                Label("Menu", systemImage: "ellipsis.circle")
            }
        }
        .navigationTitle("Directions")
        .navigationBarTitleDisplayMode(.inline)
    }
    
    func getDirections() {
        self.route = nil
                
        // Create and configure the request
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: self.startingPoint))
        request.destination = self.selectedResult
        request.transportType = selectedTransportType.type
        
        // Get the directions based on the request
        Task {
            let directions = MKDirections(request: request)
            let response = try? await directions.calculate()
            route = response?.routes.first
            getTravelTime()
        }
    }
    
    
    private func getTravelTime() {
        guard let route else { return }
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.allowedUnits = [.hour, .minute]
        travelTime = formatter.string(from: route.expectedTravelTime)
    }
}

//#Preview {
//    DestinationDetailsView()
//}
