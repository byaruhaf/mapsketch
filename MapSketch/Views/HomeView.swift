//
//  HomeView.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 16/10/2023.
//

import SwiftUI
import ExyteChat
import MapKit

struct HomeView: View {
    var ablyService: AblyServiceManager
    @StateObject private var notificationsManager = NotificationsService()
    
    var body: some View {
        TabView {
            Group {
                MapView(ablyService: ablyService)
                    .tabItem {
                        Label("Map", systemImage: "map")
                    }
                
                ChatView(messages: ablyService.messages) { draft in
                    ablyService.sendChatMessage(draft)
                }
                .tabItem {
                    Label("Chat", systemImage: "text.bubble")
                }
                
                DestinationListingView()
                .padding()
                .tabItem {
                    Label("Directions", systemImage: "arrow.up.left.and.down.right.and.arrow.up.right.and.down.left")
                }
            }
            .toolbarBackground(.indigo, for: .tabBar)
            .toolbarBackground(.visible, for: .tabBar)
            .toolbarColorScheme(.dark, for: .tabBar)
            
        }
        .onFirstAppear {
            Task {
                await notificationsManager.getAuthStatus()
                if !notificationsManager.hasPermission {
                    await notificationsManager.request()
                    await notificationsManager.getAuthStatus()
                }
            }
        }
    }
}

#Preview {
    HomeView(ablyService: AblyServiceManager())
}

enum DecodingError: Error {
    case wrongType
}
