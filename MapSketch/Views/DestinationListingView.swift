//
//  DestinationListingView.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 01/11/2023.
//

import SwiftUI
import SwiftData

struct DestinationListingView: View {
    @Query(filter: #Predicate{ !$0.name.localizedStandardContains("Current Location")}, sort: [SortDescriptor(\Destination.orderNumber, order: .reverse)]) var destinaitons: [Destination]
    @Environment(\.modelContext) var modelContext
    @State private var path = [Destination]()
    
    var body: some View {
        NavigationStack(path: $path) {
        List {
            ForEach(destinaitons) { destinaiton in
                NavigationLink(value: destinaiton) {
                    HStack {
                        Image(systemName: "mappin.circle")
                            .foregroundColor(Color(.systemRed))
                            .font(.system(size: 20))
                        VStack(alignment: .leading, spacing: 0){
                            Text(destinaiton.orderNumber.description).font(.headline)
                            Text("created by \(destinaiton.name)").font(.subheadline).fontWeight(.light)
                        }
                    }
                }
            }
            .onDelete { indexSet in
                deleteDestinations(indexSet)
            }
        }
        .environment(\.defaultMinListRowHeight, 70)
        .scrollContentBackground(.hidden)
        .navigationDestination(for: Destination.self, destination: DestinationDetailsView.init)
        .navigationTitle("Waypoints")
    }
    }
    
    func deleteDestinations(_ indexSet: IndexSet) {
        for index in indexSet {
            let destination = destinaitons[index]
            modelContext.delete(destination)
        }
    }
}

#Preview {
    DestinationListingView()
}
