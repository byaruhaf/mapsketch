//
//  AppDelegate.swift
//  MapSketch
//
//  Created by Franklin Byaruhanga on 01/11/2023.
//

import SwiftUI
import FirebaseCore
import Ably

class AppDelegate: NSObject, UIApplicationDelegate {
    var app: MapSketchApp?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        FirebaseApp.configure()
        return true
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse) async {
        let userInfo = response.notification.request.content.userInfo
        
        if let channelType = userInfo["channel_type"] {
            print("channel type from userNotificationCenter didReceive: \(channelType)")
            if let channelType = userInfo["cid"] as? String {
                Task {
                    await app?.handleDeeplinking(channelId: channelType)
                }
            }
        }
        // print("----------------userInfo-----------")
        // print(userInfo)
        // print("----------------userInfo-----------")
    }
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification) async -> UNNotificationPresentationOptions {
        let userInfo = notification.request.content.userInfo
        
        print(userInfo)
        
        // Change this to your preferred presentation option
        //        return [.sound, .badge, .banner, .list]
//        if app?.viewRouter.currentPage == .inbox {
//            return [.badge]
//        } else {
        return [.sound, .badge, .banner, .list]
//        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        ARTPush.didRegisterForRemoteNotifications(withDeviceToken: deviceToken, realtime: self.getAblyRealtime())
//        let ably = self.getAblyRealtime()
//        ably.push.activate()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        ARTPush.didFailToRegisterForRemoteNotificationsWithError(error, realtime: self.getAblyRealtime())
        // sometimes this function didFailToRegisterForRemoteNotificationsWithError is not called
#if targetEnvironment(simulator)
        print("It's because the device is simulator")
#else
        print(error.localizedDescription)
#endif
    }
    
//    func getAblyRealtime() -> ARTRealtime {
//        var options = ARTClientOptions()
//        // Set up options; API key or auth URL, etc.
//        return ARTRealtime(options: options)
//    }
}
